# ntopng

Dockerfile to build a network monitoring container using ntopng x86_64 v.3.9.x - (C) 1998-19 ntop.org built on Ubuntu 16.04.5 LTS

## container image

To run without builing the image

```
$ docker run --net=host -t -p 3000:3000 henrybravo/ntopng ntopng -i <my-network-interface> --disable-login=1
```

## build

To build the docker image

```
$ docker build -t <my-ntopng-docker-image-name> .
```

## run

```
$ docker run --net=host -t -p 3000:3000 <my-ntopng-docker-image-name> ntopng -i <my-network-interface> --disable-login=1 --community
```

ommitting "--community" starts ntopng enterprise and runs it for 10 minutes if no licence is found and then it continues to run in community edition mode.

you can also run 

```
$ docker run <my-ntopng-docker-image-name> ntopng --help
```

to see what's available in this build