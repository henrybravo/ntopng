FROM ubuntu:16.04

LABEL maintainer="Henry Bravo info@henrybravo.nl"

ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_FRONTEND teletype

RUN apt-get update -y \
&& apt-get install -y --no-install-recommends apt-utils \
&& apt-get -y -q install sudo lsb-release curl \
&& curl -s --remote-name http://packages.ntop.org/apt/16.04/all/apt-ntop.deb \
&&  sudo dpkg -i apt-ntop.deb \
&&  rm -rf apt-ntop.deb

RUN apt-get update -y \
&&  apt-get -y -q install ntopng redis-server libpcap0.8 libmysqlclient20 iptables-persistent libjson0-dev \
&&  apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 3000

RUN echo '#!/bin/bash\n/etc/init.d/redis-server start\nntopng "$@"' > /tmp/run.sh \
&& chmod +x /tmp/run.sh

ENTRYPOINT ["/tmp/run.sh"]
